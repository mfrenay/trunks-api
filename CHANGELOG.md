# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.3.0] - 2022-11-15
### Changed
- Call postTreatment method after loading data in retrieveListData and filterAndSortList API

## [1.2.2] - 2022-07-28
### Added
- Insert row API
- Call preSave method before inserting row
- Send list error message in the response in saveList API.

### Changed
- filterAndSortList method must respond with TListDto 
  - default, when param ONLY_SEND_ROWS_ON_FILTER is set to true, only put rows in TListDo
  - when param ONLY_SEND_ROWS_ON_FILTER is set to false, return complete TListDto

## [1.2.1] - 2022-07-14
### Changed
- Optimize method filterAndSortList and only respond with TListData rather than full TList
- Optimize method exportXls by ignoring the search field values

## [1.2.0] - 2021-12-28
### Feature
- Manage EXPORT_ALL custom param in exportXls to force exporting all records of a list.

## [1.1.1] - 2021-11-27
### Feature
- ResponseDTO: add new property 'errors'.
- New interface to store API response message keys.
- TrunksApiHelper: new generic method to set field values.

### Bug fix
- Check that translation xml file exists before reading it when creating user session.
- Don't log if translations don't exist

## [1.1.0] - 2021-08-29
### Feature
- Add xmlDirectory property to TrunksApiController to custom the directory where the webForm XML files.

### Bug fix
- Fix excel export: webAppPath was null when packaged in WAR (i.e., not deployed on external Tomcat)

### Security fix
- TrunksApiController: Call setCustomParams after setIdUserSession so the session params overrides the custom params.

## [1.0.10] - 2021-08-19
### Feature
- Give @Component annotation to UserSessionFactoryImpl 
- UserSessionFactoryImpl : manage translations via a singleton and so, read once translations files per language
- TrunksApiUserSession : Generic class to represent a session used by the API, this class refers Singleton to get generic and specific translations

## [1.0.9] - 2021-07-14
### Feature
- TrunksApiController.getCFormData: param primaryKeys must not be required.
- Transform null value to empty value in the method TrunksApiController.setCustomParams because value put into hashmap can't be null

## [1.0.8] - 2021-06-14
### Feature
- TrunksApiController.getFormData: add newFormFallback boolean query param. If the value is true and the form can not be loaded, send back a new form. If the value is false and the form can not be loaded, return a 404 error)

### Bug Fix
- TrunksApiController.saveFormData was unable to update value of primary key fields. (BREAKING CHANGE: new initialPkValues property in TSaveFormDataDTO)

[1.2.1]: https://gitlab.com/mfrenay/trunks-api/compare/v1.2.0...v1.2.1
[1.2.0]: https://gitlab.com/mfrenay/trunks-api/compare/v1.1.1...v1.2.0
[1.1.1]: https://gitlab.com/mfrenay/trunks-api/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/mfrenay/trunks-api/compare/v1.0.10...v1.1.0
[1.0.10]: https://gitlab.com/mfrenay/trunks-api/compare/v1.0.9...v1.0.10
[1.0.9]: https://gitlab.com/mfrenay/trunks-api/compare/v1.0.8...v1.0.9
[1.0.8]: https://gitlab.com/mfrenay/trunks-api/compare/62d271ab418ecb071cb36b7c2f12b3b4b1753d4d...v1.0.8