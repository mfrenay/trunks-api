package org.project.trunks.api.dto;

import org.project.trunks.webform.dto.TRowDTO;

public class TFilterListDTO extends TAbstractWithParamsDTO
{
    protected TRowDTO filters;


    public TFilterListDTO()
    {

    }

    public TRowDTO getFilters()
    {
        return filters;
    }

    public void setFilters(TRowDTO filters)
    {
        this.filters = filters;
    }

}
