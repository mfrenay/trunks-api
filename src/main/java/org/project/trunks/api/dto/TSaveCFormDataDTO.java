package org.project.trunks.api.dto;

import org.project.trunks.webform.dto.TComplexFormDataDTO;

public class TSaveCFormDataDTO extends TAbstractWithParamsDTO {
    protected TComplexFormDataDTO cForm;

    public TSaveCFormDataDTO() {
    }

    public TComplexFormDataDTO getCForm()
    {
        return cForm;
    }

    public void setCForm(TComplexFormDataDTO cForm)
    {
        this.cForm = cForm;
    }
}
