package org.project.trunks.api.dto;

public class TFilterAndSortListDTO
{
    protected TFilterListDTO tFilterListDTO;
    protected TSortListDTO tSortListDTO;

    public TFilterAndSortListDTO()
    {
    }

    public TFilterListDTO gettFilterListDTO()
    {
        return tFilterListDTO;
    }

    public void settFilterListDTO(TFilterListDTO tFilterListDTO)
    {
        this.tFilterListDTO = tFilterListDTO;
    }

    public TSortListDTO gettSortListDTO()
    {
        return tSortListDTO;
    }

    public void settSortListDTO(TSortListDTO tSortListDTO)
    {
        this.tSortListDTO = tSortListDTO;
    }
}
