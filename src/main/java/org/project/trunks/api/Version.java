package org.project.trunks.api;

import java.util.ArrayList;

public class Version {
    private static final ArrayList<Version> versions;

    public static ArrayList<Version> getVersions() {
        return versions;
    }

    public static Version getCurrentVersion() {
        return versions.get(versions.size() - 1);
    }

    static {
        versions = new ArrayList<>();
        versions.add(new Version("V 1.0.0 (02/03/2021)", "Project creation", "TrunksApiController, DTO, UserSessionFactoryImpl"));
        versions.add(new Version("V 1.0.1 (14/03/2021)", "Bug fix in mergeData (list must be an EditList)", "TrunksApiController"));
        versions.add(new Version("V 1.0.2 (23/03/2021)", "- In saveFormData, insert the primary key value if the field is insertable; - Manage rowState in saveFormData", "TrunksApiController"));
    }

    private final String label;
    private final String comment;
    private final String detail;

    public Version(String pLabel, String pComment, String pDetail) {
        label = pLabel;
        comment = pComment;
        detail = pDetail;
    }

    public static String getVersion() {
        return (versions.get(versions.size() - 1)).getLabel();
    }

    public String getLabel() {
        return label;
    }

    public String getComment() {
        return comment;
    }

    public String getDetail() {
        return detail;
    }
}
