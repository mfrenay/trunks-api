package org.project.trunks.api.dto;


import java.util.List;

public class ResponseDTO {
    public static final String STATUS_SUCCESS = "success";
    public static final String STATUS_WARNING = "warning";
    public static final String STATUS_ERROR = "error";

    private String status;
    private String message;
    private List<String> errors;
    private Object data;

    public ResponseDTO() {
    }

    public ResponseDTO(String status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
