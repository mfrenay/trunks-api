package org.project.trunks.api.user;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.project.trunks.api.translation.TranslationsManager;
import org.project.trunks.user.GenericUserSession;
import org.project.trunks.utilities.Translation;


public class TrunksApiUserSession extends GenericUserSession
{
    private static final Log log = LogFactory.getLog(TrunksApiUserSession.class);

    @Override
    public void setLanguage(String s)
    {
        super.setLanguage(s);

        // link global HashMap to the user, so the methods getTranslation(...) will work without any problem
        Translation translation = TranslationsManager.getInstance().getHmAllTranslationsByLanguage().get(s);
        if(translation != null)
            this.translation = translation;
        /*else
            log.error(">>>>>>>> TrunksApiUserSession.setLanguage("+s+") - global Translation object was not defined");*/
    }
}