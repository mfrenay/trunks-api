package org.project.trunks.api.factory;

import org.project.trunks.api.user.TrunksApiUserSession;
import org.project.trunks.user.GenericUserSession;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface UserSessionFactory {

    TrunksApiUserSession createUserSession(HttpServletRequest request, HttpServletResponse response);
    TrunksApiUserSession createUserSession(String language, HttpServletRequest request, HttpServletResponse response);
}
