package org.project.trunks.api.dto;

import org.project.trunks.webform.dto.TRowDTO;

public class TInsertRowDTO extends TAbstractWithParamsDTO {
    private TRowDTO row;

    public TInsertRowDTO() {
    }

    public TRowDTO getRow() {
        return row;
    }

    public void setRow(TRowDTO row) {
        this.row = row;
    }
}
