package org.project.trunks.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;
import org.project.trunks.api.constant.ResponseMessage;
import org.project.trunks.api.dto.*;
import org.project.trunks.api.factory.UserSessionFactory;
import org.project.trunks.api.factory.impl.UserSessionFactoryImpl;
import org.project.trunks.api.helper.TrunksApiHelper;
import org.project.trunks.connection.WebFormDBManager;
import org.project.trunks.data.Field;
import org.project.trunks.data.FieldDisplay;
import org.project.trunks.data.SearchField;
import org.project.trunks.generation.excel.GenExcel;
import org.project.trunks.user.GenericUserSession;
import org.project.trunks.user.SessionManager;
import org.project.trunks.utilities.DateUtil;
import org.project.trunks.utilities.ListUtil;
import org.project.trunks.utilities.StringUtilities;
import org.project.trunks.webform.*;
import org.project.trunks.webform.dto.*;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/trunks/v1/webform")
public class TrunksApiController {

    private static final Log log = LogFactory.getLog(TrunksApiController.class);

    protected UserSessionFactory userSessionFactory;

    protected String xmlDirectory = null;

    public TrunksApiController() {
        this.userSessionFactory = new UserSessionFactoryImpl();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // LIST
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * getListObject
     * @param request
     * @param response
     * @param idList
     * @param idUser
     * @return
     *
     * @apiNote (Example) http://localhost:8300/<context-path>/trunks/v1/webform/getListObject?idList=EVENT_TYPES_LIGHT&idUser=3
     */
    @GetMapping("/getListObject")
    ResponseDTO getListObject(HttpServletRequest request,
                              HttpServletResponse response,
                              @RequestParam(value = "idList") String idList,
                              @RequestParam(value = "idUser", required = false) String idUser) {
        log.debug("<<< TrunksApiController.getListObject[" + idList + ", "+ idUser +"]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            // Create session
            userSession = getUserSession(request, response);

            // Set id user session
            setIdUserSession(userSession, idUser);

            // Get list object
            StandardList list = WebFormManager.getList(idList, false, request, xmlDirectory);

            responseDTO.setData(new TListDTO(list));
            responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);

            return responseDTO;
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.getListObject[" + idList + ", "+ idUser +"]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
            return responseDTO;
        }
        finally {
            log.debug("<<< TrunksApiController.getListObject[" + idList + ", "+ idUser +"] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     * getListData
     * @param request
     * @param response
     * @param idList
     * @param idUser
     * @return
     *
     * @apiNote (Example) http://localhost:8300/<context-path>/trunks/v1/webform/getListData?idList=EVENT_TYPES_LIGHT&idUser=3
     */
    @GetMapping("/getListData")
    ResponseDTO getListData(HttpServletRequest request,
                            HttpServletResponse response,
                            @RequestParam(value = "idList") String idList,
                            @RequestParam(value = "idUser", required = false) String idUser,
                            @RequestParam(value = "params", required = false) String params) {
        log.debug("<<< TrunksApiController.getListData[" + idList + ", " + idUser + "]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            // Create session
            userSession = getUserSession(request, response);

            // Manage custom params
            TrunksApiHelper.setCustomParams(userSession, params);

            // Set id user session
            setIdUserSession(userSession, idUser);

            // Get list object
            StandardList list = WebFormManager.getList(idList, false, request, xmlDirectory);

            // Check list params
            TrunksApiHelper.checkListParams(userSession, list);

            // load data
            list.loadFromDB();
            if (!list.getErrorMessage().equals("")) {
                response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
                responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
                //responseDTO.setMessage(list.getErrorMessage()); The front-end must not receive error details from the server
                return responseDTO;
            }

            responseDTO.setData(new TListDataDTO(list));
            responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);

            return responseDTO;
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.getListData[" + idList + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
            return responseDTO;
        }
        finally {
            log.debug("<<< TrunksApiController.getListData[" + idList + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     * retrieveListAndData
     * @param request
     * @param response
     * @param idList
     * @param idUser
     * @return
     *
     * @apiNote (Example) http://localhost:8300/<context-path>/trunks/v1/webform/retrieveListAndData?idList=PATIENT_LIST&idUser=3&params=...
     */
    @PostMapping("/retrieveListAndData")
    ResponseDTO retrieveListAndData(HttpServletRequest request,
                                    HttpServletResponse response,
                                    @RequestParam(value = "idList") String idList,
                                    @RequestParam(value = "idUser", required = false) String idUser,
                                    @RequestBody TRetrieveListAndDataDTO tRetrieveListAndDataDTO) {
        log.debug("<<< TrunksApiController.retrieveListAndData[" + idList + ", " + idUser + "]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            // Create session
            userSession = getUserSession(request, response);

            // Manage custom params
            TrunksApiHelper.setCustomParams(userSession, tRetrieveListAndDataDTO.getParams());

            // Set id user session
            setIdUserSession(userSession, idUser);

            // Get list object
            StandardList list = WebFormManager.getList(idList, false, request, xmlDirectory);

            // Check list params
            TrunksApiHelper.checkListParams(userSession, list);

            // load data
            list.loadFromDB();

            // post loading
            list.postTreatment(true, request, response);

            if (!list.getErrorMessage().equals("")) {
                response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
                responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
                //responseDTO.setMessage(list.getErrorMessage()); The front-end must not receive error details from the server
                return responseDTO;
            }

            responseDTO.setData(new TListDTO(list));
            responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);

            return responseDTO;
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.retrieveListAndData[" + idList + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
            return responseDTO;
        }
        finally {
            log.debug("<<< TrunksApiController.retrieveListAndData[" + idList + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     * filterList
     * @param request
     * @param response
     * @param idList
     * @param idUser
     * @return
     *
     * @apiNote (Example) http://localhost:8300/<context-path>/trunks/v1/webform/filterList?idList=EVENT_TYPES_LIGHT&idUser=3
     */
    @PostMapping("/filterList")
    ResponseDTO filterList(HttpServletRequest request,
                           HttpServletResponse response,
                           @RequestParam(value = "idList") String idList,
                           @RequestParam(value = "idUser", required = false) String idUser,
                           @RequestBody(required = false) TFilterListDTO tFilterListDTO
    )
    {
        log.debug("<<< TrunksApiController.filterList[" + idList + ", " + idUser + "]...");
        return filterAndSortList(request, response, idList, idUser,  tFilterListDTO, null);
    }

    /**
     * filterList
     * @param request
     * @param response
     * @param idList
     * @param idUser
     * @return
     *
     * @apiNote (Example) http://localhost:8300/<context-path>/trunks/v1/webform/filterAndSortList?idList=EVENT_TYPES_LIGHT&idUser=3
     */
    @PostMapping("/filterAndSortList")
    ResponseDTO filterAndSortList(HttpServletRequest request,
                           HttpServletResponse response,
                           @RequestParam(value = "idList") String idList,
                           @RequestParam(value = "idUser", required = false) String idUser,
                           @RequestBody(required = false) TFilterAndSortListDTO tFilterAndSortListDTO
    )
    {
        log.debug("<<< TrunksApiController.filterAndSortList[" + idList + ", " + idUser + "]...");
        return filterAndSortList(request, response, idList, idUser,  tFilterAndSortListDTO.gettFilterListDTO(), tFilterAndSortListDTO.gettSortListDTO());
    }

    /**
     * filterAndSortList
     * @param request
     * @param response
     * @param idList
     * @param idUser
     * @param tFilterListDTO
     * @param tSortListDTO
     * @return ResponseDTO
     */
    ResponseDTO filterAndSortList(HttpServletRequest request,
                           HttpServletResponse response,
                           String idList,
                           String idUser,
                           TFilterListDTO tFilterListDTO,
                           TSortListDTO tSortListDTO
    ) {
        log.debug("<<< TrunksApiController.filterAndSortList[" + idList + ", " + idUser + "]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            // Create session
            userSession = getUserSession(request, response);

            // put session parameters
            if (tFilterListDTO != null)
                TrunksApiHelper.setCustomParams(userSession, tFilterListDTO.getParams());

            // Set id user session
            setIdUserSession(userSession, idUser);

            // WebFormOptions are managed in WebForm.init method
            // Maybe not necessary to load values of search fields or fields
            WebFormOptions options = new WebFormOptions();
            options.setConsiderOnlySendRowsOnFilter(true);
            // Get list object
            StandardList list = WebFormManager.getList(idList, false, request, xmlDirectory);

            list.clearSearchCriteria();

            // add filters
            if (tFilterListDTO != null) {
                list.addFilters(tFilterListDTO.getFilters());
            }

            // sort
            if (tSortListDTO != null) {
                log.debug("<<< TrunksApiController.filterAndSortList[" + idList + ", " + idUser + "] - sortFieldId = " + tSortListDTO.getSortingFieldId());
                // baseFieldSort.getName() ? No, simply use field ID
                list.setOrderByClause(tSortListDTO.getSortingFieldId() + ((tSortListDTO.getSortingOrder() > 0) ? " ASC" : " DESC"));
            }

            // load data
            list.loadFromDB();

            // post loading
            list.postTreatment(false, request, response);

            // load success ?
            if (!list.getErrorMessage().equals("")) {
                response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
                responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
                responseDTO.setMessage(list.getErrorMessage());
                return responseDTO;
            }

            TListDTO listDTO = new TListDTO(list);
            // sort
            if (tSortListDTO != null)
            {
                listDTO.setSortingFieldId(tSortListDTO.getSortingFieldId());
                listDTO.setSortingOrder(tSortListDTO.getSortingOrder());
            }
            if (StringUtilities.stringToBoolean((String)list.getHsCustomObjectParams().get(WebForm.ONLY_SEND_ROWS_ON_FILTER), true)) {
                listDTO.setBaseRow(null);   // don't send baseRow
                listDTO.setSearchRow(null); // don't send searchRow
            }
            responseDTO.setData(listDTO);

            responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);
            return responseDTO;
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.filterAndSortList[" + idList + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
            return responseDTO;
        }
        finally {
            log.debug("<<< TrunksApiController.filterAndSortList[" + idList + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     * getMatchingValues
     * @param request
     * @param response
     * @param idList
     * @param idUser
     * @return
     *
     * @apiNote (Example) http://localhost:8300/<context-path>/trunks/v1/webform/getMatchingValues?idList=AC_PATIENTS_LIGHT&idUser=3&searchText=pa
     */
    @GetMapping("/getMatchingValues")
    ResponseDTO getMatchingValues(HttpServletRequest request,
                                  HttpServletResponse response,
                                  @RequestParam(value = "idList") String idList,
                                  @RequestParam(value = "idUser", required = false) String idUser,
                                  @RequestParam(value = "searchOnlyStartingWith", defaultValue = "false") boolean searchOnlyStartingWith,
                                  @RequestParam(value = "searchText") String searchText,
                                  @RequestParam(value = "params") String params) {
        log.debug("<<< TrunksApiController.getMatchingValues[" + idList + ", " + idUser +  ", " + searchText + "]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            if (searchText == null || "".equals(searchText)) {
                throw new Exception("The request param 'searchText' is required!");
            }

            // Create session
            userSession = getUserSession(request, response);

            // Set custom params
            TrunksApiHelper.setCustomParams(userSession, params);

            // Set id user session
            setIdUserSession(userSession, idUser);

            // Get list object
            StandardList list = WebFormManager.getList(idList, false, request, xmlDirectory);

            TRow rowSearchField = list.getRowSearchField();
            String whereClause = "";
            list.setVSearchFieldParam(new ArrayList());

            // Set filter
            String fieldId = "#SEARCH_EXPRESSION#";
            String fieldValue = searchText;
            SearchField searchField = (SearchField) rowSearchField.getField(fieldId);

            if (searchField != null) {
                searchField.setValue(fieldValue);

                if (searchOnlyStartingWith) {
                    whereClause += " AND LOWER(" + searchField.getName() + ") LIKE LOWER(CONCAT(?, '%'))";
                }
                else {
                    whereClause += " AND LOWER(" + searchField.getName() + ") LIKE LOWER(CONCAT('%', ?, '%'))";
                }

                list.getVSearchFieldParam().add(searchField);
            }

            list.setInnerSearchFieldWhereClause(whereClause);
            list.loadFromDB();

            //////
            // Construct a custom JSON response
            //////

            List vRow = list.getTTable().getVRow();

            // The HashMap<String,String> is translated into json {"key": "value", ...}
            ArrayList<HashMap<String, String>> responseData = new ArrayList<>();

            for (int i = 0; i < vRow.size(); i++) {
                TRow row = (TRow) vRow.get(i);
                HashMap<String, String> json = new HashMap<>();

                for (Field field : row.getFields()) {
                    if (StringUtilities.stringToBoolean((String)field.getCustomObjectParams("SERIALIZABLE"), true)) {
                        json.put(field.getID(), field.getValue());
                    }
                }

                responseData.add(json);
            }

            // Example of responseDate:
            // [
            //      {
            //          "field ID 1": "field value 1",
            //          "field ID 2": "field value 2"
            //      },
            //      {
            //          "field ID 1": "field value 1",
            //          "field ID 2": "field value 2"
            //      }
            // ]
            responseDTO.setData(responseData);
            responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);

            return responseDTO;
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.getMatchingValues[" + idList + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
            return responseDTO;
        }
        finally {
            log.debug("<<< TrunksApiController.getMatchingValues[" + idList + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     * saveList
     *
     * Note:
     * tListDto is received as a JSON object and converted automatically by Jackson into a TListDTO
     *
     * @param request
     * @param response
     * @param idList
     * @param idUser
     * @param tListDTO
     * @return
     *
     * @apiNote (Example) http://localhost:8300/<context-path>/trunks/v1/webform/saveList?idList=AC_PATIENTS_LIGHT&idUser=3
     */
    @PostMapping("/saveList")
    ResponseDTO saveList(HttpServletRequest request,
                         HttpServletResponse response,
                         @RequestParam(value = "idList") String idList,
                         @RequestParam(value = "idUser", required = false) String idUser,
                         @RequestBody TListDTO tListDTO,
                         @RequestParam(value = "params") String params) {
        log.debug("<<< TrunksApiController.saveList[" + idList + ", " + idUser +"]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            // Create session
            userSession = getUserSession(request, response);

            // Set custom params
            TrunksApiHelper.setCustomParams(userSession, params);

            // Set id user session
            setIdUserSession(userSession, idUser);

            // Get list object
            EditList list = (EditList)WebFormManager.getList(idList, true, request, xmlDirectory);

            // Add received data in the list
            for (TRowDTO tRowDTO : tListDTO.getRows()) {
                TRow row = list.getTTable().duplicateBaseRow();

                for (Field field : row.getFields()) {
                    field.setFieldValue(THelperDTO.getFieldValue(tRowDTO, field.getID()));
                }

                list.getTTable().addRow(row);
            }

            boolean stopOnError = StringUtilities.stringToBoolean((String) list.getHsCustomObjectParams().get("MERGE_DATA.STOP_ON_ERROR"), false);
            ArrayList<TRowTreated> rowsTreated = new WebFormDBManager().mergeData(list, userSession, null, stopOnError);

            boolean hasError = false;
            for (TRowTreated rowTreated : rowsTreated) {
                log.debug("<<< TrunksApiController.saveList[" + idList + ", " + idUser + "]["+rowTreated.getRow()+"].status => " + rowTreated.getStatus());

                if (!hasError && WebFormDBManager.MERGE_STATUS_ERROR.equals(rowTreated.getStatus())) {
                    hasError = true;
                }
            }

            if (hasError) {
                response.setStatus(HttpStatus.SC_BAD_REQUEST);
                responseDTO.setStatus(ResponseDTO.STATUS_WARNING);
                //responseDTO.setMessage(list.getErrorMessage());  The front-end must not receive error details from the server
                return responseDTO;
            }

            if (!list.getErrorMessage().equals("")) {
                response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
                responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
                responseDTO.setMessage(list.getErrorMessage());
                return responseDTO;
            }

            responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);

            return responseDTO;
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.saveList[" + idList + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
            return responseDTO;
        }
        finally {
            log.debug("<<< TrunksApiController.saveList[" + idList + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     * deleteRow
     *
     * @param request
     * @param response
     * @param idList
     * @param idUser
     * @param tDeleteRowDTO
     * @return
     */
    @DeleteMapping("/deleteRow")
    ResponseDTO deleteRow(HttpServletRequest request,
                          HttpServletResponse response,
                          @RequestParam(value = "idList") String idList,
                          @RequestParam(value = "idUser", required = false) String idUser,
                          @RequestBody TDeleteRowDTO tDeleteRowDTO) {
        log.debug("<<< TrunksApiController.deleteRow[" + idList + ", " + idUser + "]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            // Create session
            userSession = getUserSession(request, response);

            // Manage custom params
            TrunksApiHelper.setCustomParams(userSession, tDeleteRowDTO.getParams());

            // Set id user session
            setIdUserSession(userSession, idUser);

            // It is not necessary to load values of search fields
            // But we need to manage search fields to get their expression to build where clause for this criteria
            WebFormOptions options = new WebFormOptions();
            options.setLoadSearchFieldValues(false);
            options.setLoadFieldValues(false);
            EditList list = (EditList) WebFormManager.getList(idList, true, request, xmlDirectory, options);

            if (list == null) {
                throw new Exception("The list with ID '" + idList + "' was not found!");
            }

            // Check list params
            TrunksApiHelper.checkListParams(userSession, list);

            TRow baseRow = list.getBaseRow();
            String whereClause = "";
            list.setVSearchFieldParam(new ArrayList());

            // Get all primary keys from the Base row
            List<Field> pkFields = baseRow.getPkFields();

            // Retrieve all primary key values from the HashMap mapPrimaryKeys
            // and build a where clause based on these values
            for (Field pkField : pkFields) {
                String pkValue = THelperDTO.getFieldValue(tDeleteRowDTO.getRowPkFields(), pkField.getID());

                whereClause += " AND " + pkField.getName() + " = ?";

                SearchField searchField = new SearchField();
                searchField.setValue(pkValue);
                list.getVSearchFieldParam().add(searchField);
            }

            list.setInnerSearchFieldWhereClause(whereClause);
            list.loadFromDB();

            if (list.getTTable().getVRow().size() != 1) {
                throw new Exception("Row to delete can not be retrieved!");
            }

            TRow rowToDelete = (TRow) list.getTTable().getVRow().get(0);
            int nbDeletedRows = list.delete(request, rowToDelete);
            if (!list.getErrorMessage().equals("")) {
                log.error("<<< TrunksApiController.deleteRow[" + idList + ", " + idUser + "] - ERROR : " + list.getErrorMessage());
                response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
                responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
                responseDTO.setMessage(list.getErrorMessage());  // send this message
                return responseDTO;
            }

            // Build a custom JSON response
            HashMap<String, Object> responseData = new HashMap<>();
            responseData.put("nbDeletedRows", nbDeletedRows);

            responseDTO.setData(responseData);
            responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);

            return responseDTO;
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.deleteRow[" + idList + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(form.getErrorMessage()); The front-end must not receive error details from the server
            return responseDTO;
        }
        finally {
            log.debug("<<< TrunksApiController.deleteRow[" + idList + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     * insertRow
     *
     * @param request
     * @param response
     * @param idList
     * @param idUser
     * @param tInsertRowDTO
     * @return ResponseDTO
     */
    @PutMapping("/insertRow")
    ResponseDTO insertRow(HttpServletRequest request,
                          HttpServletResponse response,
                          @RequestParam(value = "idList") String idList,
                          @RequestParam(value = "idUser", required = false) String idUser,
                          @RequestBody TInsertRowDTO tInsertRowDTO) {
        log.debug("<<< TrunksApiController.insertRow[" + idList + ", " + idUser + "]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            // Create session
            userSession = getUserSession(request, response);

            // Manage custom params
            TrunksApiHelper.setCustomParams(userSession, tInsertRowDTO.getParams());

            // Set id user session
            setIdUserSession(userSession, idUser);

            // It is not necessary to load values of search fields
            // But we need to manage search fields to get their expression to build where clause for this criteria
            WebFormOptions options = new WebFormOptions();
            options.setLoadSearchFieldValues(false);
            options.setLoadFieldValues(false);
            EditList list = (EditList) WebFormManager.getList(idList, true, request, xmlDirectory, options);

            if (list == null) {
                throw new Exception("The list with ID '" + idList + "' was not found!");
            }

            // Check list params
            TrunksApiHelper.checkListParams(userSession, list);

            // Set row values
            TRow rowToAdd = list.getRowForAdd();
            for (int k = 0; k < rowToAdd.getFields().length; k++) {
                Field fieldToAdd = rowToAdd.getField(k);
                String insertRowFieldValue = THelperDTO.getFieldValue(tInsertRowDTO.getRow().getFields(), fieldToAdd.getID());
                if (insertRowFieldValue != null) {
                    fieldToAdd.setValue(StringUtilities.getEString(insertRowFieldValue, fieldToAdd.getValue()));
                }
            }

            Connection conn = null;
            WebFormDBManager db = new WebFormDBManager();
            int rc = 0;
            try {
                // get connection
                conn = db.getConnection();

                // pre save validation
                if (list.preSave(true, rowToAdd, request, conn)) {
                    // if pre save ok => insert row
                    rc = db.insertRow(list, rowToAdd, conn);
                }

                // commit
                if (conn != null)
                    conn.commit();
            }
            catch (Throwable e) {
                if (conn != null)
                    conn.rollback();
            }
            finally {
                if (conn != null)
                    conn.close();
                conn = null;
                db = null;
            }

            if (rc != 1) {
                log.error("<<< TrunksApiController.insertRow[" + idList + ", " + idUser + "] - ERROR : " + list.getErrorMessage());
                response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
                responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
                responseDTO.setMessage(StringUtilities.getEString(list.getErrorMessage(), "Impossible to add this row"));
                return responseDTO;
            }

            // Build a custom JSON response
            HashMap<String, Object> responseData = new HashMap<>();

            responseDTO.setData(responseData);
            responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);

            return responseDTO;
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.insertRow[" + idList + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(form.getErrorMessage()); The front-end must not receive error details from the server
            return responseDTO;
        }
        finally {
            log.debug("<<< TrunksApiController.insertRow[" + idList + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     * switchRow
     *
     * @param request
     * @param response
     * @param idList
     * @param idUser
     * @param tSwitchRowDTO
     * @return
     */
    @PostMapping("/switchRow")
    ResponseDTO switchRow(HttpServletRequest request,
                          HttpServletResponse response,
                          @RequestParam(value = "idList") String idList,
                          @RequestParam(value = "idUser", required = false) String idUser,
                          @RequestParam(value = "direction") String direction, // [UP / DOWN]
                          @RequestBody TSwitchRowDTO tSwitchRowDTO) {
        log.debug("<<< TrunksApiController.switchRow[" + idList + ", " + idUser + "]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            if (StringUtilities.getNString(direction).equals(""))
                throw new Exception("The switch direction is not provided");
            if (!direction.equals("UP") && !direction.equals("DOWN"))
                throw new Exception("The switch direction '" + direction + "' is not valid (UP/DOWN)");

            String operatorForOrderField = " < ";
            String sortForOrderField = " DESC ";
            if (direction.equals("DOWN")) {
                operatorForOrderField = " > ";
                sortForOrderField = " ASC ";
            }

            // Create session
            userSession = getUserSession(request, response);

            // Set custom params
            TrunksApiHelper.setCustomParams(userSession, tSwitchRowDTO.getParams());

            // Set id user session
            setIdUserSession(userSession, idUser);

            EditList list = (EditList) WebFormManager.getList(idList, true, request, xmlDirectory);

            if (list == null) {
                throw new Exception("The list with ID '" + idList + "' was not found!");
            }

            TRow baseRow = list.getBaseRow();
            String whereClause = "";
            list.setVSearchFieldParam(new ArrayList());

            // ---------------------------------------------------------------------------------------------------------
            // WHICH IS THE FIELD MANAGING THE ORDER
            // ---------------------------------------------------------------------------------------------------------
            Field fieldORDER = null;
            for (Field field : baseRow.getFields()) {
                if (field.getDisplayType().equals(FieldDisplay.ORDER)) {
                    fieldORDER = field;
                    break;
                }
            }
            if (fieldORDER == null)
                throw new Exception("Field managing the order of this list can not be retrieved");

            // ---------------------------------------------------------------------------------------------------------
            // ROW TO UPDATE
            // ---------------------------------------------------------------------------------------------------------
            // Get all primary keys from the Base row
            List<Field> pkFields = baseRow.getPkFields();

            // Retrieve all primary key values from the HashMap mapPrimaryKeys
            // and build a where clause based on these values
            for (Field pkField : pkFields) {
                String pkValue = THelperDTO.getFieldValue(tSwitchRowDTO.getRowPkFields(), pkField.getID());

                whereClause += " AND " + pkField.getName() + " = ?";

                SearchField searchField = new SearchField();
                searchField.setValue(pkValue);
                list.getVSearchFieldParam().add(searchField);
            }

            list.setInnerSearchFieldWhereClause(whereClause);
            list.loadFromDB();

            if (list.getTTable().getVRow().size() != 1) {
                throw new Exception("Row to move " + direction + " can not be retrieved!");
            }

            TRow rowSelected = (TRow) list.getTTable().getVRow().get(0);

            log.debug("<<< TrunksApiController.switchRow[" + idList + ", " + idUser + "] - rowSelected = " + rowSelected.toString());

            // ---------------------------------------------------------------------------------------------------------
            // ROW TO SWITCH
            // ---------------------------------------------------------------------------------------------------------
            list.getVSearchFieldParam().clear();

            // Build a where clause based on the field order
            list.setInnerSearchFieldWhereClause(" AND " + fieldORDER.getName() + operatorForOrderField + " ?");
            list.getVSearchFieldParam().add(rowSelected.getField(fieldORDER.getID()));
            list.setOrderByClause(fieldORDER.getName() + sortForOrderField + "LIMIT 1");

            list.loadFromDB();

            if (list.getTTable().getVRow().size() != 1) {
                throw new Exception("Row to switch can not be retrieved!");
            }

            TRow rowToSwitch = (TRow) list.getTTable().getVRow().get(0);

            log.debug("<<< TrunksApiController.switchRow[" + idList + ", " + idUser + "] - rowToSwitch = " + rowToSwitch.toString());
            // ---------------------------------------------------------------------------------------------------------

            // apply switch
            new WebFormDBManager().switchRow(list, rowSelected, rowToSwitch, fieldORDER, null);

            // clear filter
            list.setInnerSearchFieldWhereClause("");
            list.getVSearchFieldParam().clear();
            list.setOrderByClause(fieldORDER.getName() + " ASC");

            // load data
            list.loadFromDB();

            responseDTO.setData(new TListDTO(list));
            responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);

            return responseDTO;
        } catch (Throwable e) {
            log.error("<<< TrunksApiController.switchRow[" + idList + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
            return responseDTO;
        } finally {
            log.debug("<<< TrunksApiController.switchRow[" + idList + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // FORM
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * getNewFormData
     *
     * @param request
     * @param response
     * @param idForm
     * @param idUser
     * @return
     *
     * @apiNote (Example) http://localhost:8300/<context-path>/trunks/v1/webform/getNewFormData?idForm=PATIENT_LIGHT&idUser=3
     */
    @GetMapping("/getNewFormData")
    ResponseDTO getNewFormData(HttpServletRequest request,
                               HttpServletResponse response,
                               @RequestParam(value = "idForm") String idForm,
                               @RequestParam(value = "idUser", required = false) String idUser,
                               @RequestParam(value = "params", required = false) String params) {
        log.debug("<<< TrunksApiController.getNewFormData[" + idForm + ", " + idUser + "]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            // Create session
            userSession = getUserSession(request, response);

            // Set custom params
            TrunksApiHelper.setCustomParams(userSession, params);

            // Set id user session
            setIdUserSession(userSession, idUser);

            // Get form object
            Form form = WebFormManager.getForm(idForm, request, xmlDirectory);

            if (form == null) {
                throw new Exception("Form with id '" + idForm + "' not found!");
            }

            form.loadForNewRow(request);

            // Seb-Mich: 15/04/2021 - The default values are managed in the clearFields() method called inside loadForNewRow()
            // Set default values in the TForm's row
            /*for (int i = 0; i < form.getBaseRow().getFields().length; i++) {
                Field field = form.getBaseRow().getFields()[i];

                // If there is a default value defined for the field, copy it in the field of the Row
                if (!"".equals(field.getDefaultValue())) {
                    form.setFieldValue(field.getID(), field.getDefaultValue());
                }
            }*/

            // Load values for parameterized LOV
            form.initParametersOfLOVField(form.getBaseRow().getFields(), form.getTRow().getFields());

            responseDTO.setData(new TFormDTO(form));
            responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);

            return responseDTO;
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.getNewFormData[" + idForm + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
            return responseDTO;
        }
        finally {
            log.debug("<<< TrunksApiController.getNewFormData[" + idForm + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     * getFormData
     *
     * @param request
     * @param response
     * @param idForm
     * @param idUser
     * @return
     *
     * @apiNote (Example) http://localhost:8300/<context-path>/trunks/v1/webform/getFormData?idForm=PATIENT_LIGHT&idUser=3&primaryKeyValue=1
     */
    @GetMapping("/getFormData")
    ResponseDTO getFormData(HttpServletRequest request,
                            HttpServletResponse response,
                            @RequestParam(value = "idForm") String idForm,
                            @RequestParam(value = "idUser", required = false) String idUser,
                            // Must be pass in the order defined in form.xml. In the future, we could pass a Map to have a key/value pair for the PK
                            @RequestParam(value = "primaryKeys") String strPrimaryKeysFields,
                            @RequestParam(value = "newFormFallback", defaultValue = "true") Boolean newFormFallback,
                            @RequestParam(value = "params", required = false) String params) {
        log.debug("<<< TrunksApiController.getFormData[" + idForm + ", " + idUser +  ", " + strPrimaryKeysFields + "]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            TFieldDTO[] primaryKeysFieldDTOs = new ObjectMapper().readValue(strPrimaryKeysFields, TFieldDTO[].class);

            if (primaryKeysFieldDTOs == null || primaryKeysFieldDTOs.length == 0) {
                throw new Exception("The request param 'primaryKeys' is required!");
            }

            log.debug("<<< TrunksApiController.getFormData - primaryKeys: " + primaryKeysFieldDTOs);

            // Create session
            userSession = getUserSession(request, response);

            // Set custom params
            TrunksApiHelper.setCustomParams(userSession, params);

            // Set id user session
            setIdUserSession(userSession, idUser);

            // Get form object
            Form form = WebFormManager.getForm(idForm, request, xmlDirectory);

            if (form == null) {
                throw new Exception("Form with id '" + idForm + "' not found!");
            }

            List<Field> primaryKeyFields = new ArrayList<>();

            for (TFieldDTO primaryKeysFieldDTO : primaryKeysFieldDTOs) {
                primaryKeyFields.add(new Field(primaryKeysFieldDTO.getId(), primaryKeysFieldDTO.getValue()));
            }

            // set primary key(s)
            form.setPkValues(primaryKeyFields);
            // load data
            form.loadFromDB();

            if (!form.getErrorMessage().equals("")) {
                TrunksApiHelper.manageWebFormError(form, response, responseDTO);
                return responseDTO;
            }

            // TODO: UserSession.hasWebformAccess()

            if (!form.isLoadSuccess()) {
                log.debug("<<< TrunksApiController.getFormData - form " + idForm + " - load success is false");

                if (newFormFallback) {
                    log.debug("<<< TrunksApiController.getFormData - form " + idForm + " - newFormFallback is true => send back a new form");
                    form.loadForNewRow(request);
                } else {
                    log.debug("<<< TrunksApiController.getFormData - form " + idForm + " - newFormFallback is false => send back a new form");

                    response.setStatus(HttpStatus.SC_NOT_FOUND);
                    responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
                    //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
                    return responseDTO;
                }
            }

            responseDTO.setData(new TFormDTO(form));
            responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);

            return responseDTO;
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.getFormData[" + idForm + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
            return responseDTO;
        }
        finally {
            log.debug("<<< TrunksApiController.getFormData[" + idForm + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     * saveFormData
     *
     * Example (mapForm):
     * {
     *     "ID_EVENT_TYPE": "2",
     *     "TITLE": "Prise de sang",
     *     "DURATION": "00:15",
     *     "YN_DURATION_EDITABLE": "N",
     *     "BACKGROUND_COLOR": "#ff534b",
     *     "TEXT_COLOR": "#ffffff",
     *     "ICON": "fas fa-tint"
     * }
     *
     * @param request
     * @param response
     * @param idForm
     * @param idUser
     * @param tSaveFormDataDTO
     * @return
     */
    @PostMapping("/saveFormData")
    ResponseDTO saveFormData(HttpServletRequest request,
                             HttpServletResponse response,
                             @RequestParam(value = "idForm") String idForm,
                             @RequestParam(value = "idUser", required = false) String idUser,
                             @RequestBody TSaveFormDataDTO tSaveFormDataDTO) {
        log.debug("<<< TrunksApiController.saveFormData[" + idForm + ", " + idUser + "]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            // Create session
            userSession = getUserSession(request, response);

            // Set the params in user session
            TrunksApiHelper.setCustomParams(userSession, tSaveFormDataDTO.getParams());

            // Set id user session
            setIdUserSession(userSession, idUser);

            Form form = WebFormManager.getForm(idForm, request, xmlDirectory);

            if (form == null) {
                throw new Exception("The form with ID '" + idForm + "' was not found!");
            }

            // Get all primary keys from the Base row
            List<Field> pkBaseFields = form.getBaseRow().getPkFields();
            String[] pkValues = new String[pkBaseFields.size()];
            String[] pkInitialValues = new String[pkBaseFields.size()];
            String sAllPkValues = "";

            // Retrieve all primary key values from the HashMap mapForm
            for (int i = 0; i < pkBaseFields.size(); i++) {
                Field pkField = pkBaseFields.get(i);

                String pkValue = StringUtilities.getNString(THelperDTO.getFieldValue(tSaveFormDataDTO.getRow(), pkField.getID()));
                pkValues[i] = pkValue;
                pkInitialValues[i] = StringUtilities.getEString(THelperDTO.getFieldValue(tSaveFormDataDTO.getInitialValues(), pkField.getID()), pkValue);
                sAllPkValues += pkValue;
            }

            // If we have defined the FORCE_INSERT to Y, force the insert
            if (TRowState.NEW.equals(tSaveFormDataDTO.getRowState())
                    || StringUtilities.stringToBoolean((String) form.getCustomObjectParams("FORCE_INSERT"))
            ) {
                form.loadForNewRow(request);

                // Set the primary keys in the Form and load data from DB
                form.setPkValues(pkValues);
            }
            // load data before merge with client values
            // New Form
            else if ("".equals(sAllPkValues)) {
                form.loadForNewRow(request);
            }
            // Existing form
            else {
                // Set the primary keys in the Form and load data from DB
                form.setPkValues(pkInitialValues);
                form.loadFromDB();

                if (!form.isLoadSuccess()) {
                    log.debug("<<< TrunksApiController.saveFormData - form " + idForm + " - load success is false");
                    form.loadForNewRow(request);
                }
            }

            // Set field values (+ get errors)
            List<String> fieldValueErrors = TrunksApiHelper.setFieldsValues(form, tSaveFormDataDTO);

            // If at least one field has a value error
            if (!fieldValueErrors.isEmpty()) {
                response.setStatus(HttpStatus.SC_BAD_REQUEST);
                responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
                responseDTO.setMessage(ResponseMessage.FORM_DATA_ERROR);
                responseDTO.setErrors(fieldValueErrors);
                return responseDTO;
            }

            // Save the form data in DB
            form.save(request, false, null);

            // is there an error ?
            if (!form.getErrorMessage().equals("")) {
                TrunksApiHelper.manageWebFormError(form, response, responseDTO);
                return responseDTO;
            }

            // is there a warning ?
            if (!StringUtilities.getNString(form.getWarningMessage()).equals("")) {
                responseDTO.setStatus(ResponseDTO.STATUS_WARNING);
                responseDTO.setMessage(form.getWarningMessage());
            }
            // no warning nor error
            else {
                responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);
            }

            // Build a custom JSON response
            HashMap<String, HashMap<String, String>> responseData = new HashMap<>();
            HashMap<String, String> primaryKeys = new HashMap<>();

            for (Field pkField : pkBaseFields) {
                primaryKeys.put(pkField.getID(), form.getFieldValue(pkField.getID()));
            }
            responseData.put("primaryKeys", primaryKeys);

            responseDTO.setData(responseData);

            return responseDTO;
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.saveFormData[" + idForm + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
            return responseDTO;
        }
        finally {
            log.debug("<<< TrunksApiController.saveFormData[" + idForm + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     * getCFormData
     *
     * @param request
     * @param response
     * @param idCForm
     * @param idUser
     * @return
     *
     * @apiNote (Example) http://localhost:8300/<context-path>/trunks/v1/webform/getCFormData?idForm=C_PATIENT_LIGHT&idUser=3&primaryKeyValue=1
     */
    @GetMapping("/getCFormData")
    ResponseDTO getCFormData(HttpServletRequest request,
                             HttpServletResponse response,
                             @RequestParam(value = "idCForm") String idCForm,
                             @RequestParam(value = "idUser", required = false) String idUser,
                             @RequestParam(value = "primaryKeys", required = false) String strPrimaryKeysFields,
                             @RequestParam(value = "params", required = false) String params) {
        log.debug("<<< TrunksApiController.getCFormData[" + idCForm + ", " + idUser +  ", " + strPrimaryKeysFields + "]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            // Create session
            userSession = getUserSession(request, response);

            // Set custom params
            TrunksApiHelper.setCustomParams(userSession, params);

            // Set id user session
            setIdUserSession(userSession, idUser);

            // Get CForm object
            CompleteForm cForm = WebFormManager.getCompleteForm(idCForm, request, xmlDirectory);

            if (cForm == null) {
                throw new Exception("CForm with id '" + idCForm + "' not found!");
            }

            // set primary key(s)
            TRow primaryKeysTRow = null;
            if (strPrimaryKeysFields != null) {
                TFieldDTO[] primaryKeysFieldDTOs = new ObjectMapper().readValue(strPrimaryKeysFields, TFieldDTO[].class);
                if (primaryKeysFieldDTOs != null) {
                    Field[] fields = new Field[primaryKeysFieldDTOs.length];

                    for (int i = 0; i < primaryKeysFieldDTOs.length; i++) {
                        fields[i] = new Field(primaryKeysFieldDTOs[i].getId(), primaryKeysFieldDTOs[i].getValue());
                    }
                    primaryKeysTRow = new TRow(fields);
                }
            }

            // load data
            cForm.loadFromDB(primaryKeysTRow, request);

            if (!cForm.getErrorMessage().equals("")) {
                TrunksApiHelper.manageWebFormError(cForm, response, responseDTO);
                return responseDTO;
            }

            // TODO: UserSession.hasWebformAccess()

            if (!cForm.isLoadSuccess()) {
                log.debug("<<< TrunksApiController.getCFormData - cForm " + cForm + " - load success is false");
                //form.loadForNewRow(request);
            }
            else {
                log.debug("<<< TrunksApiController.getCFormData - cForm " + cForm + " - load success is true");
            }

            responseDTO.setData(new TComplexFormDTO(cForm));
            responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);

            return responseDTO;
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.getCFormData[" + idCForm + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
            return responseDTO;
        }
        finally {
            log.debug("<<< TrunksApiController.getCFormData[" + idCForm + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     * exportXls
     * @param request
     * @param response
     * @param idList
     * @param idUser
     * @param tFilterAndSortListDTO
     * @return
     *
     * @apiNote (Example) http://localhost:8300/<context-path>/trunks/v1/webform/exportXls
     */
    @PostMapping("/exportXls")
    ResponseEntity<Resource> exportXls(HttpServletRequest request,
                                       HttpServletResponse response,
                                       @RequestParam(value = "idList") String idList,
                                       @RequestParam(value = "idUser", required = false) String idUser,
                                       @RequestBody(required = false) TFilterAndSortListDTO tFilterAndSortListDTO
    ) {
        log.debug("<<< TrunksApiController.exportXls[" + idList + ", " + idUser + "]...");

        GenericUserSession userSession = null;

        TFilterListDTO tFilterListDTO = tFilterAndSortListDTO.gettFilterListDTO();
        TSortListDTO tSortListDTO = tFilterAndSortListDTO.gettSortListDTO();

        try {
            // Create session
            userSession = getUserSession(request, response);

            // put session parameters
            if (tFilterListDTO != null)
                TrunksApiHelper.setCustomParams(userSession, tFilterListDTO.getParams());

            // Set id user session
            setIdUserSession(userSession, idUser);

            // Get list object
            // NO NEED TO LOAD VALUES OF SEARCH FIELDS
            WebFormOptions options = new WebFormOptions();
            options.setLoadSearchFieldValues(false);
            StandardList list = WebFormManager.getList(idList, false, request, xmlDirectory, options);

            list.clearSearchCriteria();

            // add filters
            if (tFilterListDTO != null)
                list.addFilters(tFilterListDTO.getFilters());

            // sort
            if (tSortListDTO != null) {
                log.debug("<<< TrunksApiController.exportXls[" + idList + ", " + idUser + "] - sortFieldId = " + tSortListDTO.getSortingFieldId());
                // baseFieldSort.getName() ? No, simply use field ID
                list.setOrderByClause(tSortListDTO.getSortingFieldId() + ((tSortListDTO.getSortingOrder() > 0) ? " ASC" : " DESC"));
            }

            boolean svgRetrieveAll = list.isRetrieveAll();

            // Force retrieve all records if EXPORT_ALL=Y
            if (StringUtilities.stringToBoolean((String) list.getHsCustomObjectParams().get("EXPORT_ALL"), false)) {
                list.setRetrieveAll(true);
            }

            try {
                // load data
                list.loadFromDB();
            } finally {
                list.setRetrieveAll(svgRetrieveAll);
            }

            // load success ?
            if (!list.getErrorMessage().equals("")) {
                response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
                return null;
            }

            /*------------------------
             * Create temp Excel file
             */
            String pathGen = null;
            String webAppPath = request.getSession().getServletContext().getRealPath("");

            if (webAppPath == null) {
                String temporaryDirectory = "output";
                new File("." + File.separator + temporaryDirectory).mkdir();
                pathGen = "output" + File.separator;
            } else {
                String rootPath = webAppPath.replaceAll("/", File.separator) //"\\\\")
                        + File.separator;
                pathGen = rootPath + "output" + File.separator;
            }
            String validName = list.getID().toLowerCase(Locale.ROOT);

            String fileName = pathGen + validName + "_" + new DateUtil().getUniqueTime() + ".xls";
            ByteArrayResource resource;

            try {
                // Gestion des donnees
                ArrayList vRecord = new ArrayList();
                Field[] titles = list.processReportData("LIST", vRecord);

                // createListingFile
                new GenExcel(fileName).createListingFile(titles, vRecord);

                Path path = Paths.get(fileName);
                resource = new ByteArrayResource(Files.readAllBytes(path));
            }
            finally {
                // Delete temporary file
                File tempFile = new File(fileName);

                if (tempFile.exists()) {
                    boolean isDeleted = tempFile.delete();
                    log.debug("<<< TrunksApiController.exportXls - delete success (" + isDeleted + ") - file " + fileName);
                }
            }

            /*------------------------
             * Excel (end)
             */

            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.exportXls[" + idList + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            //responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
            return null;
        }
        finally {
            log.debug("<<< TrunksApiController.exportXls[" + idList + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param idList
     * @param idUser
     * @param file
     * @return
     */
    @PostMapping("/importXls")
    public ResponseDTO importXls(HttpServletRequest request,
                          HttpServletResponse response,
                          @RequestParam(value = "idList") String idList,
                          @RequestParam(value = "idUser", required = false) String idUser,
                          @RequestParam("file") MultipartFile file,
                          @RequestParam(value = "params", required = false) String params
    ) {
        log.debug("<<< TrunksApiController.importXls[" + idList + ", " + idUser + "]...");

        ResponseDTO responseDTO = new ResponseDTO();
        GenericUserSession userSession = null;

        try {
            // Create session
            userSession = getUserSession(request, response);

            // Set custom params
            TrunksApiHelper.setCustomParams(userSession, params);

            // Set id user session
            setIdUserSession(userSession, idUser);

            // Get list object
            EditList list = (EditList) WebFormManager.getList(idList, true, request, xmlDirectory);

            ArrayList<String> errorMessages = new ArrayList<>();

            // Import data from file into the list
            boolean importSuccess = list.importXLS(file.getInputStream(), request, response, errorMessages);

            if (!importSuccess) {
                response.setStatus(HttpStatus.SC_BAD_REQUEST);
                responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
                responseDTO.setMessage("Could not import excel file!");
                responseDTO.setData(errorMessages);
                return responseDTO;
            }

            responseDTO.setStatus(ResponseDTO.STATUS_SUCCESS);
            return responseDTO;
        }
        catch (Throwable e) {
            log.error("<<< TrunksApiController.importXls[" + idList + ", " + idUser + "]...", e);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
            //responseDTO.setMessage(e.getMessage()); The front-end must not receive error details from the server
            return responseDTO;
        }
        finally {
            log.debug("<<< TrunksApiController.importXls[" + idList + ", " + idUser + "] - END");

            if (request != null) {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                SessionManager.getInstance().removeSession(userSession);
            }
            userSession = null;
        }
    }

    /**
     * Set ID user session
     *
     * @param userSession
     * @param idUser
     * @throws Exception
     */
    protected void setIdUserSession(GenericUserSession userSession, String idUser) throws Exception {
        userSession.setID_User(idUser);
    }

    /**
     * Get the User Session
     *
     * @param request
     * @param response
     */
    protected GenericUserSession getUserSession(HttpServletRequest request,
                                                HttpServletResponse response) {
        GenericUserSession userSession = SessionManager.getInstance().getSession(request);
        if (userSession == null) { // Create session
            userSession = userSessionFactory.createUserSession(request, response);
        }

        return userSession;
    }
}
