package org.project.trunks.api.factory.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.project.trunks.api.factory.UserSessionFactory;
import org.project.trunks.api.translation.TranslationsManager;
import org.project.trunks.api.user.TrunksApiUserSession;
import org.project.trunks.user.SessionManager;
import org.project.trunks.utilities.FileUtilities;
import org.project.trunks.utilities.Translation;
import org.springframework.stereotype.Component;
import org.xml.sax.InputSource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStream;

@Component
public class UserSessionFactoryImpl implements UserSessionFactory {

    private static final Log log = LogFactory.getLog(UserSessionFactoryImpl.class);

    /**
     * Create a session with language code FR
     * @param request
     * @param response
     * @return
     */
    @Override
    public TrunksApiUserSession createUserSession(HttpServletRequest request, HttpServletResponse response) {
        return  createUserSession("FR", request, response);
    }

    /**
     * Create a session
     * @param language
     * @param request
     * @param response
     * @return
     */
    @Override
    public TrunksApiUserSession createUserSession(String language, HttpServletRequest request, HttpServletResponse response) {
        // Session id
        String userSessionId = request.getSession().getId();

        TrunksApiUserSession user = this.instantiateGenericUserSession();

        user.setSessionID(userSessionId);

        // Store translations in a map available for all sessions
        if (TranslationsManager.getInstance().getHmAllTranslationsByLanguage().get(language) == null) {
            try {
                // Fill translation object with generic and specific translations
                Translation translation = user.readTranslations(request, language);

                // Put this object into the Singleton
                TranslationsManager.getInstance().getHmAllTranslationsByLanguage().put(language, translation);
            }
            catch (Exception e) {
                log.warn("<<<< UserSessionFactoryImpl.createUserSession() - error reading translation file - " + e.getMessage());
            }
        }

        // must be done after creating and putting Translation object into the Singleton
        user.setLanguage(language);

        // Add session object to the session request
        request.getSession().setAttribute("Session", user);

        // Add session object to Session manager
        SessionManager.getInstance().addSession(user);

        return user;
    }

    protected TrunksApiUserSession instantiateGenericUserSession() {
        return new TrunksApiUserSession();
    }
}
