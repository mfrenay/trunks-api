package org.project.trunks.api.dto;

import org.project.trunks.webform.dto.TFieldDTO;
import org.project.trunks.webform.dto.TRowDTO;

import java.util.ArrayList;

public class TSaveFormDataDTO extends TAbstractWithParamsDTO {
    protected TRowDTO row;
    protected ArrayList<TFieldDTO> initialValues;
    protected String rowState;

    public TSaveFormDataDTO() {
    }

    public TRowDTO getRow() {
        return row;
    }

    public void setRow(TRowDTO row) {
        this.row = row;
    }

    public ArrayList<TFieldDTO> getInitialValues() {
        return initialValues;
    }

    public void setInitialValues(ArrayList<TFieldDTO> initialValues) {
        this.initialValues = initialValues;
    }

    public String getRowState() {
        return rowState;
    }

    public void setRowState(String rowState) {
        this.rowState = rowState;
    }
}
