package org.project.trunks.api.dto;

import org.project.trunks.webform.dto.TRowDTO;

public abstract class TAbstractWithParamsDTO
{
    protected TRowDTO params;

    public TAbstractWithParamsDTO()
    {

    }

    public TRowDTO getParams()
    {
        return params;
    }

    public void setParams(TRowDTO params)
    {
        this.params = params;
    }
}
