package org.project.trunks.api.dto;

import org.project.trunks.webform.dto.TRowDTO;

public class TDeleteRowDTO extends TAbstractWithParamsDTO {
    private TRowDTO rowPkFields;

    public TDeleteRowDTO() {
    }

    public TRowDTO getRowPkFields() {
        return rowPkFields;
    }

    public void setRowPkFields(TRowDTO rowPkFields) {
        this.rowPkFields = rowPkFields;
    }
}
