package org.project.trunks.api.dto;

import org.project.trunks.webform.dto.TRowDTO;

public class TSwitchRowDTO extends TAbstractWithParamsDTO {
    private TRowDTO rowPkFields;

    public TSwitchRowDTO() {
    }

    public TRowDTO getRowPkFields() {
        return rowPkFields;
    }

    public void setRowPkFields(TRowDTO rowPkFields) {
        this.rowPkFields = rowPkFields;
    }
}
