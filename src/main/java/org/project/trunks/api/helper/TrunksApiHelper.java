package org.project.trunks.api.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;
import org.project.trunks.api.dto.ResponseDTO;
import org.project.trunks.api.dto.TSaveFormDataDTO;
import org.project.trunks.data.Field;
import org.project.trunks.data.FieldType;
import org.project.trunks.user.GenericUserSession;
import org.project.trunks.utilities.StringUtilities;
import org.project.trunks.webform.Form;
import org.project.trunks.webform.StandardList;
import org.project.trunks.webform.TRowState;
import org.project.trunks.webform.WebForm;
import org.project.trunks.webform.dto.TFieldDTO;
import org.project.trunks.webform.dto.THelperDTO;
import org.project.trunks.webform.dto.TRowDTO;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class TrunksApiHelper {

    private static final Log log = LogFactory.getLog(TrunksApiHelper.class);

    /**
     * Set the custom params of a request in the user session object
     *
     * @param userSession The user session
     * @param params A JSON formatted String containing the params
     * @throws JsonProcessingException
     */
    public static void setCustomParams(GenericUserSession userSession, String params) throws JsonProcessingException {
        // Manage custom params
        if (! StringUtilities.getNString(params).equals("")) {
            TRowDTO rowDTO = new ObjectMapper().readValue(params, TRowDTO.class);
            setCustomParams(userSession, rowDTO);
        }
    }

    /**
     * Set the custom params of a request in the user session object
     *
     * @param userSession The user session
     * @param params Hashmap containing the params
     */
    public static void setCustomParams(GenericUserSession userSession, TRowDTO params)  {
        // Manage custom params
        if (params != null) {
            for (TFieldDTO elem : params.getFields()) {
                String key = elem.getId();
                String value = StringUtilities.getNString(elem.getValue()); // value put into hashmap can't be null

                log.debug("TrunksApiController.setCustomParams(#" + key + "#, " + value + ")");
                userSession.getHsCustomParams().put("#" + key + "#", value);
            }
        }
    }

    /**
     * Check if all required param of a StandardList are present in the userSession object.
     *
     * @param userSession The user session object
     * @param list The list in which checking the params
     * @throws Exception In case a parameter is missing in the userSession
     */
    public static void checkListParams(GenericUserSession userSession, StandardList list) throws Exception {
        List<Field> listParams = list.getVFieldParam();
        String[] frameworkParams = new String[] {"#ID_USER#", "#LANGUAGE#"};

        if (listParams != null) {
            for (Field field : listParams) {
                if (StringUtilities.findIn(frameworkParams, field.getStartValue())) {
                    continue;
                }

                // Check if the field param's value is linked to a userSession param
                if (!userSession.getHsCustomParams().containsKey(field.getStartValue())) {
                    // The parameter is not in the session
                    // Check if there is a default value for this param
                    if (field.getXmlObject().getAttributeValue("DEFAULT_VALUE") != null) {
                        field.setValue(field.getDefaultValue());
                        continue;
                    }

                    if (!field.getID().equals("") && field.getStartValue().equals("")) {
                        // It is a param linked to a field ID, and not linked to a userSession param
                        continue;
                    }

                    // Missing param
                    throw new Exception("Missing param '" + field.getStartValue() + "'");
                }
            }
        }
    }


    /**
     *
     * @param form
     * @param tSaveFormDataDTO
     *
     * @return A list of String describing errors. An empty list if there is no error.
     */
    public static List<String> setFieldsValues(Form form, TSaveFormDataDTO tSaveFormDataDTO) throws Exception {
        List<Field> allBaseFields = form.getBaseRow().getVFields();
        List<String> fieldValueErrors = new ArrayList<>();

        // Set values from mapForm into the form
        for (Field field : allBaseFields) {

            if (field.isPrimaryKey() && !field.isInsertable()) continue;
            if (!field.isVisible()) continue;

            String fieldId = field.getID();

            String fieldValue = StringUtilities.getNString(THelperDTO.getFieldValue(tSaveFormDataDTO.getRow(), fieldId));

            // Check field value
            String error = TrunksApiHelper.checkFieldValue(form, fieldId, fieldValue);
            if (error != null) {
                fieldValueErrors.add(error);
            }

            // Update the row status if the field value has changed
            boolean hasChanged = form.setFieldValue(fieldId, fieldValue);
            log.debug("Field ID: '" + fieldId + "': " + hasChanged);
            if (hasChanged && form.getState().equals(TRowState.UNCHANGED)) {
                form.getTRow().setRowState(TRowState.MODIFIED);
            }
        }

        return fieldValueErrors;
    }

    /**
     * Check if the field has an accepted value
     * @param form
     * @param fieldId The ID of the field
     * @param fieldValue The value of the field to check
     */
    public static String checkFieldValue(Form form, String fieldId, String fieldValue) {
        log.info("TrunksApiController.checkFieldValue(" + fieldId + ", " + fieldValue + ")");

        Field field = form.getField(fieldId);
        FieldType fieldType = field.getType();

        if (StringUtilities.getNString(fieldValue).equals("")) {
            if (field.isMandatory()) {
                log.error("TrunksApiController.checkFieldValue - mandatory field '" + fieldId + "' has a null value");
                return "The field with ID '" + fieldId + "' is mandatory";
            }
            else {
                return null;
            }
        }

        if (FieldType.INTEGER.equals(fieldType)) {
            int fieldIntVal;
            try {
                fieldIntVal = Integer.parseInt(fieldValue);
            } catch (NumberFormatException exception) {
                log.error("TrunksApiController.checkFieldValue - unparseable integer (" + fieldValue + ")");
                return "The field with ID '" + fieldId + "' must be an integer but the value is (" + fieldValue + ")";
            }

            Hashtable customObjectParams = field.getHsCustomObjectParams();

            // Check MIN value
            String minValueParam = StringUtilities.getNString((String) customObjectParams.get("MIN"));
            if (!minValueParam.equals("")) {
                int minValue = Integer.parseInt(minValueParam);
                if (fieldIntVal < minValue) {
                    return "The field with ID '" + fieldId + "' must have a value greater than (" + minValue + ") but is equal to (" + fieldIntVal + ")";
                }
            }

            // Check MAX value
            String maxValueParam = StringUtilities.getNString((String) customObjectParams.get("MAX"));
            if (!maxValueParam.equals("")) {
                int maxValue = Integer.parseInt(maxValueParam);
                if (fieldIntVal > maxValue) {
                    return "The field with ID '" + fieldId + "' must have a value smaller than (" + maxValue + ") but is equal to (" + fieldIntVal + ")";
                }
            }

            // Check STEP value
            String stepValueParam = StringUtilities.getNString((String) customObjectParams.get("STEP"));
            if (!stepValueParam.equals("")) {
                int stepValue = Integer.parseInt(stepValueParam);
                if (fieldIntVal % stepValue != 0) {
                    return "The field with ID '" + fieldId + "' must have a value that is a multiple of (" + stepValue + ") but is equal to (" + fieldIntVal + ")";
                }
            }
        }

        return null;
    }

    /**
     * Check error message and code in the given webForm, and set these values in the given response and respondDTO
     *
     * @param webForm
     * @param response
     * @param responseDTO
     */
    public static void manageWebFormError(WebForm webForm, HttpServletResponse response, ResponseDTO responseDTO) {
        if (!"".equals(StringUtilities.getNString(webForm.getErrorCode()))) {
            try {
                response.setStatus(Integer.parseInt(webForm.getErrorCode()));
            }
            catch (Throwable e) {
                response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            }
        }
        else {
            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
        responseDTO.setStatus(ResponseDTO.STATUS_ERROR);
        responseDTO.setMessage(webForm.getErrorMessage());
    }
}
