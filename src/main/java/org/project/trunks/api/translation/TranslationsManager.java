package org.project.trunks.api.translation;

import org.project.trunks.utilities.Translation;

import java.util.HashMap;

public class TranslationsManager
{
    static TranslationsManager instance;

    HashMap<String, Translation> hmAllTranslationsByLanguage = new HashMap<>();

    public static TranslationsManager getInstance()
    {
        if(instance == null)
        {
            instance = new TranslationsManager();
        }
        return instance;
    }

    public HashMap<String, Translation> getHmAllTranslationsByLanguage()
    {
        return hmAllTranslationsByLanguage;
    }
}
