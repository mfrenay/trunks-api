package org.project.trunks.api.dto;

public class TSortListDTO extends TAbstractWithParamsDTO
{
    protected String sortingFieldId;
    protected int sortingOrder;

    public TSortListDTO()
    {
    }

    public String getSortingFieldId()
    {
        return sortingFieldId;
    }

    public void setSortingFieldId(String sortingFieldId)
    {
        this.sortingFieldId = sortingFieldId;
    }

    public int getSortingOrder()
    {
        return sortingOrder;
    }

    public void setSortingOrder(int sortingOrder)
    {
        this.sortingOrder = sortingOrder;
    }
}
